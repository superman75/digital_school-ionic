import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from "@ionic-native/status-bar";
import { SplashScreen } from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {ProfilePage} from "../pages/profile/profile";
import {ClassPage} from "../pages/class/class";
import {DashStartPage} from "../pages/dash-start/dash-start";
import {SocialPage} from "../pages/social/social";
import {CommentsPage} from "../pages/comments/comments";
import {NewsPage} from "../pages/news/news";
import {SearchPage} from "../pages/search/search";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = SearchPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.hide();
      statusBar.overlaysWebView(true);
      splashScreen.hide();
    });
  }

  logOut() {
    this.nav.setRoot(LoginPage);
  }
  openProfile() {
    this.nav.setRoot(ProfilePage);
  }
  openDashBoard(){
    this.nav.setRoot(DashStartPage);
  }

  openGuida() {
    this.nav.setRoot(ClassPage,{index : 0,selector : 'vocabulary'});
  }
  openSocial() {
    this.nav.setRoot(SocialPage);
  }
}
