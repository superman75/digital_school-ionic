import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import {LoginPage} from "../pages/login/login";
import {DashStartPage} from "../pages/dash-start/dash-start";
import {ProfilePage} from "../pages/profile/profile";
import {ClassPage} from "../pages/class/class";
import {Ionic2RatingModule} from "ionic2-rating";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {NewsPage} from "../pages/news/news";
import {TaskPage} from "../pages/task/task";
import {SocialPage} from "../pages/social/social";
import {ChartModule} from "angular2-highcharts";
import solidgauge from 'highcharts/modules/solid-gauge';
import more from 'highcharts/highcharts-more';
import * as highcharts from 'Highcharts'
import {CommentsPage} from "../pages/comments/comments";
import {SearchPage} from "../pages/search/search";
more(highcharts);
solidgauge(highcharts);

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    DashStartPage,
    ProfilePage,
    ClassPage,
    NewsPage,
    TaskPage,
    SocialPage,
    CommentsPage,
    SearchPage
  ],
  imports: [
    BrowserModule,
    Ionic2RatingModule,
    PdfViewerModule,
    ChartModule.forRoot(highcharts),
    IonicModule.forRoot(MyApp,{
      platforms:{
        ios:{
          statusbarPadding : true
        }
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    DashStartPage,
    ProfilePage,
    ClassPage,
    NewsPage,
    TaskPage,
    SocialPage,
    CommentsPage,
    SearchPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
