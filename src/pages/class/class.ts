import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {DashStartPage} from "../dash-start/dash-start";


/**
 * Generated class for the ClassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-class',
  templateUrl: 'class.html',
})
export class ClassPage {

  private vocList : any[] = [
    {
      title : 'INTERNET OF THINGS',
      icon : 'assets/imgs/IOT_icon.png',
      desc : 'Sit amet, consectetur adipisci Sit amet ' +
      'consectetur adipisci.' +
      'Sit amet, consectetur adipisc' +
      'Sit amet, consectetur   ',
      passed : false
    },
    {
      title : 'INTERNET OF THINGS',
      icon : 'assets/imgs/IOT_icon.png',
      desc : 'Sit amet, consectetur adipisci Sit amet ' +
      'consectetur adipisci.' +
      'Sit amet, consectetur adipisc' +
      'Sit amet, consectetur   ',
      passed : false
    },
    {
      title : 'INTERNET OF THINGS',
      icon : 'assets/imgs/IOT_icon.png',
      desc : 'Sit amet, consectetur adipisci Sit amet ' +
      'consectetur adipisci.' +
      'Sit amet, consectetur adipisc' +
      'Sit amet, consectetur   ',
      passed : true
    },
    {
      title : 'INTERNET OF THINGS',
      icon : 'assets/imgs/IOT_icon.png',
      desc : 'Sit amet, consectetur adipisci Sit amet ' +
      'consectetur adipisci.' +
      'Sit amet, consectetur adipisc' +
      'Sit amet, consectetur   ',
      passed : true
    },
  ]
  private esameList : any[] = [
    {
      icon : 'video',
      title : 'TECHNO1',
      rate : 3,
      desc : 'Video-sit amet , consectetur adpiscing elit'
    },
    {
      icon : 'video',
      title : 'TECHNO2',
      rate : 4,
      desc : 'Video-sit amet , consectetur adpiscing elit'},
    {
      icon : 'video',
      title : 'TECHNO2',
      rate : 5,
      desc : 'Video-sit amet , consectetur adpiscing elit'},
    {
      icon : 'pdf',
      title : 'TRENDS1',
      rate : 3,
      desc : "Video-sit amet , consectetur adpiscing elit Video-sit amet , consectetur adpiscing elit"}

  ];

  private news : any[] = [
    {
      date : '1 APRILE 2018',
      photoUrl : 'assets/imgs/news_1.jpeg',
      desc : 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ' +
      'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips' +
      'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ' +
      'lorem ipsum lorem ipsum lorem ipsum '
    },
    {
      date : '20 MARCH 2018',
      photoUrl : 'assets/imgs/news_2.jpg',
      desc : 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ' +
      'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips' +
      'lorem ipsum lorem ipsum lorem ipsum '
    },
    {
      date : '10 MARCH 2018',
      photoUrl : 'assets/imgs/news_3.jpg',
      desc : 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ' +
      'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips' +
      'lorem ipsum lorem ipsum lorem ipsum '
    },
    {
      date : '28 FEBRUARY 2018',
      photoUrl : 'assets/imgs/news_4.jpg',
      desc : 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ' +
      'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips' +
      'lorem ipsum lorem ipsum lorem ipsum '
    }
  ];
  private tasks : any[] = [
    {
      desc : 'Sit amet, consectetur adipisci ' +
      'Sit amet consectetur adipisci ' +
      'Sit amet, consectetur adipisc ' +
      'Sit amet, consectetur   ',
      point  : 380,
      passed : true
    },
    {
      desc : 'Sit amet, consectetur adipisci ' +
      'Sit amet consectetur adipisci ' +
      'Sit amet, consectetur adipisc ' +
      'Sit amet, consectetur   ',
      point  : 380,
      passed : false
    },
    {
      desc : 'Sit amet, consectetur adipisci ' +
      'Sit amet consectetur adipisci ' +
      'Sit amet, consectetur adipisc ' +
      'Sit amet, consectetur   ',
      point  : 380,
      passed : false
    },
    {
      desc : 'Sit amet, consectetur adipisci ' +
      'Sit amet consectetur adipisci ' +
      'Sit amet, consectetur adipisc ' +
      'Sit amet, consectetur   ',
      point  : 380,
      passed : false
    }
  ]
  private pdfSrc: string = 'assets/pdf/pdf-test.pdf';
  private index  : number = 0;
  private progress : number = 80;
  private rate : number = 3;
  private selector : string = 'vocabulary';
  private selList : string[] = ['vocabulary','news','task','esame'];
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private modalCtrl : ModalController) {
    this.index = this.navParams.get('index');
    this.selector = this.navParams.get('selector');
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad ClassPage');
    // this.selector = 'esame';
  }
  showSelector() : void {
    console.log(this.selector);
  }

  openDashBoard() {
    this.navCtrl.setRoot(DashStartPage);
  }
}
