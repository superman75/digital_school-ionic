import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DashStartPage} from "../dash-start/dash-start";

/**
 * Generated class for the CommentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
})
export class CommentsPage {

  private event : any = {
    photoUrl : 'assets/imgs/blank_profile.jpg',
    name : 'Glullo Rossi',
    weeks : '3',
    content : '        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum' +
    '        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum' +
    '        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum' +
    '        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    imgUrl : '',
    num_favor : 2,
    num_comments : 3
  };
it
  private comments : any[] = [
    {
      photoUrl : 'assets/imgs/blank_profile.jpg',
      name: 'Marlo Rossl',
      weeks : '3',
      content : 'Lorem ipsum Lorem ipsum Lorem ipsum'
    },
    {  photoUrl : 'assets/imgs/blank_profile.jpg',
      name: 'Marlo Rossl',
      weeks : '3',
      content : 'Lorem ipsum Lorem ipsum Lorem ipsum'
    },
    {  photoUrl : 'assets/imgs/blank_profile.jpg',
      name: 'Marlo Rossl',
      weeks : '3',
      content : 'Lorem ipsum Lorem ipsum Lorem ipsum '
    },
    {  photoUrl : 'assets/imgs/blank_profile.jpg',
      name: 'Marlo Rossl',
      weeks : '3',
      content : 'Lorem ipsum Lorem ipsum Lorem ipsum'
    }
  ]
  private favor_flag  :any = '0';
  private comment_flag : any = '0';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentsPage');
  }

  openDashBoard() {
    this.navCtrl.setRoot(DashStartPage);
  }
  takeMeFavorite() {
    if(this.favor_flag == '1') {
      this.favor_flag = '0';
    } else {
      this.favor_flag = '1';
    }
  }

  takeMeComment() {
    if(this.comment_flag == '1') {
      this.comment_flag = '0';
    } else {
      this.comment_flag = '1';
    }
  }
}
