import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashStartPage } from './dash-start';

@NgModule({
  declarations: [
    DashStartPage,
  ],
  imports: [
    IonicPageModule.forChild(DashStartPage),
  ],
})
export class DashStartPageModule {}
