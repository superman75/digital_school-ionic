import { Component } from '@angular/core';
import {
  IonicPage, MenuController, ModalController, NavController, NavParams, Platform,
} from 'ionic-angular';
import {ProfilePage} from "../profile/profile";
import {StatusBar} from '@ionic-native/status-bar';

/**
 * Generated class for the DashStartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dash-start',
  templateUrl: 'dash-start.html',
})
export class DashStartPage {
  private profile : any = {
    photoUrl :  'assets/imgs/blank_profile.jpg',
    name : 'Dr. Mario rossi',
    role : 'Marketing',
  };
  private progress : number = 80;
  private isComplete : boolean = false;

  public items = [1,2,3,4];
  public card_Back_b = [{
    title : 'TRENDS AND TECHNOLOGY',
    desc : 'Totale punteggio 200pt',
    step : 0
  },
    {
      title : 'COLLOABORATION AND DIGITAL WORKSPACE',
      desc : 'Totale punteggio 200pt',
      step : 0
    },
    {
      title : 'DIGITAL REPUTATION',
      desc : 'Totale punteggio 200pt',
      step : 0
    },
    {
      title : 'HEALTHCARE DISRUPTION',
      desc : 'Totale punteggio 200pt',
      step : 0
    }];
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private modalCtrl : ModalController,
              private platform: Platform,private statusBar: StatusBar) {
    this.statusBar.hide();

  }

  ionViewDidLoad() {
    if (this.card_Back_b[0].step && this.card_Back_b[1].step && this.card_Back_b[2].step
    && this.card_Back_b[3].step) this.isComplete = true;
  }

  openPopup() {
    let popUp = this.modalCtrl.create('PopupPage');
    popUp.present();
  }

  openProfile() {
    this.navCtrl.setRoot(ProfilePage);
  }
}
