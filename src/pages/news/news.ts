import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ProfilePage} from "../profile/profile";

/**
 * Generated class for the NewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  private news : any[] = [
    {
      date : '1 APRILE 2018',
      photoUrl : 'assets/imgs/news_1.jpeg',
      desc : 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ' +
      'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips' +
      'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ' +
      'lorem ipsum lorem ipsum lorem ipsum '
    },
    {
      date : '20 MARCH 2018',
      photoUrl : 'assets/imgs/news_2.jpg',
      desc : 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ' +
      'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips' +
      'lorem ipsum lorem ipsum lorem ipsum '
    },
    {
      date : '10 MARCH 2018',
      photoUrl : 'assets/imgs/news_3.jpg',
      desc : 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ' +
      'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips' +
      'lorem ipsum lorem ipsum lorem ipsum '
    },
    {
      date : '28 FEBRUARY 2018',
      photoUrl : 'assets/imgs/news_4.jpg',
      desc : 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ' +
      'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips' +
      'lorem ipsum lorem ipsum lorem ipsum '
    }
  ]
  private profile : any = {
    photoUrl :  'assets/imgs/blank_profile.jpg',
    name : 'Dr. Mario rossi',
    role : 'Marketing',
  };
  private progress : number = 83;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private modalCtrl : ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewsPage');
  }

  openProfile() {
    this.navCtrl.setRoot(ProfilePage);
  }
}
