import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {ProfilePage} from "../profile/profile";
import {StatusBar} from '@ionic-native/status-bar';
import {DashStartPage} from "../dash-start/dash-start";

/**
 * Generated class for the PopupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popup',
  templateUrl: 'popup.html',
})
export class PopupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl : ViewController,
              private statusBar : StatusBar) {
    this.statusBar.overlaysWebView(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopupPage');
  }

  closePopup() {
    this.viewCtrl.dismiss();
  }
  openProfile() {
    // this.navCtrl.push(DashStartPage);
    this.closePopup();
  }
}
