import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, Platform} from 'ionic-angular';
import {ClassPage} from "../class/class";
import {DashStartPage} from "../dash-start/dash-start";
import {StatusBar} from "@ionic-native/status-bar";
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  styles : ['chart {display : block; width : auto; height: auto}'
  ]
})
export class ProfilePage {
  chartOptions : any;
  chartOptions1 : any;
  percentage : number = 60;
 constructor(public navCtrl: NavController, public navParams: NavParams,
              private modalCtrl : ModalController,
             public platform : Platform, public statusBar : StatusBar) {
   this.statusBar.overlaysWebView(false);

   this.chartOptions={
     chart: {
       type: 'areaspline',
       // width : 100,
       height : (8/15)*100+'%',
       borderColor : '#65cada',
       marker : {
         enabled : true,
         fillColor : '#308f9c',
         radius : 10
       }
     },
     title: {
       text: ''
     },
     yAxis: {
       title: {
         text: ''
       },
       opposite : true,
       labels : {
         enabled : false
       }
     },
     legend: {
       layout: 'horizontal',
       align: 'left',
       verticalAlign: 'top',
       x: 1000,
       y: 100,
       floating: true,
       borderWidth: 1,
     },
     xAxis: {
       categories: [
         'LUN',
         'MAR',
         'MER',
         'GIO',
         'VEN',
         'SAB',
         'DOM'
       ]
     },
     plotOptions: {
       areaspline: {
         fillOpacity: 1,
         lineColor : '#65cada'
       }
     },
     series: [{
       name: '',
       data: [2, 1, 4, {
          y : 2.3,
         marker : {
            symbol : 'url(assets/imgs/round_marker.png)'
         }
       }, 3.5, 2.5, 2.5]
     }],
     colors : [
       "#bbe4eb"
     ]
   };

   this.chartOptions1 = {
     chart: {
       type: 'solidgauge',
       height: (16/16*100)+'%'
     },
     title: '',
     pane: {
       center: ['50%', '50%'],
       size: '100%',
       startAngle: 30,
       endAngle: -210,
       background: {
         // backgroundColor: (highcharts.theme && highcharts.theme.background2) || '#CCC',
         innerRadius: '65%',
         outerRadius: '100%',
         shape: 'arc'
       }
     },
     tooltip: {
       enabled: false
     },
     yAxis: {
       min: 0,
       max: 100,
       lineWidth : 0,
       minorTickInterval: null,
       labels : {
         enabled : false
       }
     },
     series: [{
       data: [{
         y : this.percentage ,
         innerRadius : '65%'
       }],
       dataLabels :{
         enabled : false
       }
     }],

     credits: {
       enabled: false
     },
     colors : [
       '#65cada'
     ],
   }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');

  }
  openVideo() {
    this.navCtrl.setRoot(ClassPage,{index : 1});
  }
  openTest() {
    this.navCtrl.setRoot(ClassPage,{index : 3});
  }
  openTask() {
    this.navCtrl.setRoot(ClassPage,{index : 0,selector : 'task'});
  }
  openDashBoard() {
   this.navCtrl.setRoot(DashStartPage);
  }
}
