import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DashStartPage} from "../dash-start/dash-start";

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  private results : any[] = [
    {desc : 'Sit amet, consectetur adipisci Sit amet consectetur adipisci ' +
    'Sit amet, consectetur adipisci Sit amet consectetur adipisci'},
    {desc : 'Sit amet, consectetur adipisci Sit amet consectetur adipisci ' +
    'Sit amet, consectetur adipisci Sit amet consectetur adipisci'},
    {desc : 'Sit amet, consectetur adipisci Sit amet consectetur adipisci ' +
    'Sit amet, consectetur adipisci Sit amet consectetur adipisci'},
    {desc : 'Sit amet, consectetur adipisci Sit amet consectetur adipisci ' +
    'Sit amet, consectetur adipisci Sit amet consectetur adipisci'},
    {desc : 'Sit amet, consectetur adipisci Sit amet consectetur adipisci ' +
    'Sit amet, consectetur adipisci Sit amet consectetur adipisci'},
    {desc : 'Sit amet, consectetur adipisci Sit amet consectetur adipisci ' +
    'Sit amet, consectetur adipisci Sit amet consectetur adipisci'},
    {desc : 'Sit amet, consectetur adipisci Sit amet consectetur adipisci ' +
    'Sit amet, consectetur adipisci Sit amet consectetur adipisci'},
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }
  openDashBoard() {
    this.navCtrl.setRoot(DashStartPage);
  }
}
