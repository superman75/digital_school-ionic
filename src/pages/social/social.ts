import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ProfilePage} from "../profile/profile";
import {StatusBar} from "@ionic-native/status-bar";

/**
 * Generated class for the SocialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-social',
  templateUrl: 'social.html',
})
export class SocialPage {
  private events : any[] = [
    {
      photoUrl : 'assets/imgs/blank_profile.jpg',
      name : 'Glullo Rossi',
      weeks : '3',
      content : '        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum\n' +
      '        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum\n' +
      '        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum\n' +
      '        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
      imgUrl : '',
      num_favor : 2,
      num_comments : 3
    },
    {  photoUrl : 'assets/imgs/blank_profile.jpg',
      name : 'Mario Rossi',
      weeks : '3',
      content : 'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
      imgUrl : 'assets/imgs/sample.jpg',
      num_favor : 0,
      num_comments : 0
    },
    {  photoUrl : 'assets/imgs/blank_profile.jpg',
      name : 'Mario Rossi',
      weeks : '4',
      content : 'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum\n',
      imgUrl : '',
      num_favor : 1,
      num_comments : 1
    },
    {  photoUrl : 'assets/imgs/blank_profile.jpg',
      name : 'Glullo Rossi',
      weeks : '4',
      content : '        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum\n' +
      '        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum\n' +
      '        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum\n',
      imgUrl : '',
      num_favor : 5,
      num_comments : 3,
      comment_flag : '0',
      favor_flag : '0'
    }
  ];
  private content : string = '';
  private favor_flag  :any = '0';
  private comment_flag : any = '0';
  private profile : any = {
    photoUrl :  'assets/imgs/blank_profile.jpg',
    name : 'Dr. Mario rossi',
    role : 'Marketing',
  };
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private modalCtrl : ModalController, private statusBar : StatusBar) {
    this.statusBar.overlaysWebView(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SocialPage');
  }

  openProfile() {
    this.navCtrl.setRoot(ProfilePage);
  }

  takeMeFavorite(index : number) {
    if(this.events[index].favor_flag == '1') {
      this.events[index].favor_flag = '0';
    } else {
      this.events[index].favor_flag = '1';
    }
  }

  takeMeComment(index :  number) {
    if(this.events[index].comment_flag == '1') {
      this.events[index].comment_flag = '0';
    } else {
      this.events[index].comment_flag = '1';
    }
  }
}
