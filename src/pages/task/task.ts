import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ProfilePage} from "../profile/profile";

/**
 * Generated class for the TaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-task',
  templateUrl: 'task.html',
})
export class TaskPage {

  private tasks : any[] = [
    {
      desc : 'Sit amet, consectetur adipisci ' +
      'Sit amet consectetur adipisci ' +
      'Sit amet, consectetur adipisc ' +
      'Sit amet, consectetur   ',
      point  : 380,
      passed : true
    },
    {
      desc : 'Sit amet, consectetur adipisci ' +
      'Sit amet consectetur adipisci ' +
      'Sit amet, consectetur adipisc ' +
      'Sit amet, consectetur   ',
      point  : 380,
      passed : false
    },
    {
      desc : 'Sit amet, consectetur adipisci ' +
      'Sit amet consectetur adipisci ' +
      'Sit amet, consectetur adipisc ' +
      'Sit amet, consectetur   ',
      point  : 380,
      passed : false
    },
    {
      desc : 'Sit amet, consectetur adipisci ' +
      'Sit amet consectetur adipisci ' +
      'Sit amet, consectetur adipisc ' +
      'Sit amet, consectetur   ',
      point  : 380,
      passed : false
    }
  ]
  private profile : any = {
    photoUrl :  'assets/imgs/blank_profile.jpg',
    name : 'Dr. Mario rossi',
    role : 'Marketing',
  };
  private progress : number = 83;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private modalCtrl : ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskPage');
  }

  openProfile() {
    this.navCtrl.setRoot(ProfilePage);
  }

}
